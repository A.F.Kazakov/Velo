/**
 * @author	Казаков Андрей
 * @date		10.12.15.
 */

#ifndef UTILITY_TYPES_H
#define UTILITY_TYPES_H

#include <cstdint>

namespace utility
{
	using null = std::nullptr_t;

	using s8		= int8_t;
	using s16	= int16_t;
	using s32	= int32_t;
	using s64	= int64_t;

	using u8		= uint8_t;
	using u16	= uint16_t;
	using u32	= uint32_t;
	using u64	= uint64_t;
}
#endif //UTILITY_TYPES_H
